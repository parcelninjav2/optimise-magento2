<?php

namespace Optimize\Parcelninja\Gateway\Parcelninja;
use \Optimize\Parcelninja\Gateway\Parcelninja\Client;
use \Optimize\Parcelninja\Helper\Data;
use \Magento\Framework\DataObject;
use \Magento\Framework\HTTP\ZendClient;
use Magento\Quote\Model\Quote\Address\RateRequest;


class Request extends DataObject{

	/**
	 * HTTP Client
	 * @var client
	 */
	protected $client;

	/**
	 * Helper
	 * @var \Optimize\Parcelninja\Helper\Data
	 */
	protected $helper;

	/**
	 *
	 * @param \Optimize\Parcelninja\Model\Gateway\Parcelninja\Api\Client $client
	 * @param \Optimize\Parcelninja\Logger\Logger $logger
	 */
	public function __construct(
		Client $client,
		Data $helper
	) {
		$this->client = $client;
		$this->helper = $helper;
	}

	public function sendRequest($endpoint , $payload = []){

		try {
            if (empty($payload)) {
                $response = $this->client->makeRequest($endpoint, ZendClient::GET);
            } else {
                $response = $this->client->makeRequest($endpoint, ZendClient::POST, $payload);
            }
		} catch (Exception $e) {
			$this->helper->debug("Exception", $e->getMessage());

			return [];
		}
		return $response;
	}



}
