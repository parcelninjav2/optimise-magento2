<?php

namespace Optimize\Parcelninja\Gateway;

use \Magento\Framework\DataObject;
use \Optimize\Parcelninja\Logger\Logger;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Optimize\Parcelninja\Model\GatewayInterface;
use \Magento\Framework\App\Filesystem\DirectoryList;
use \Magento\Store\Model\StoreManager;

abstract class AbstractGateway extends DataObject implements GatewayInterface
{

    protected $_name = '';


    /**
     * @var \Magento\Store\Model\StoreManager
     */
    protected $_storeManager;

    /**
     * @var \Optimize\Sms\Logger\Logger
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     *
     */
    protected $scopeConfig;

    /**
     * @var \Optimize\Sms\Helper\Data
     */
    protected $_helper;


}
