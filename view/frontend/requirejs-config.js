var config = {
	map: {
		'*':{
            'Magento_Checkout/js/model/shipping-service' : 'Optimize_Parcelninja/js/model/shipping-service',
			'Magento_Checkout/js/model/shipping-save-processor/default':'Optimize_Parcelninja/js/model/shipping-save-processor/default',
			'Magento_Checkout/js/model/shipping-save-processor/payload-extender':'Optimize_Parcelninja/js/model/shipping-save-processor/payload-extender'
		  }
	}
};
